# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# roles = Role.create([{name: 'user'}, {name: 'admin'}, {name: 'moder'}])

User.create(login: 'system', password: '$y$tem', role: Role.find_by(name: 'admin'))
