class AddPublishedToArticles < ActiveRecord::Migration
  def change
    add_column            :articles, :published, :boolean, default: false
    change_column_default :articles, :archive, false
  end
end
