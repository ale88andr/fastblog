class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string          :title, index: true, unique: true
      t.text            :contents
      t.belongs_to      :author, index: true
      t.boolean         :archive
      t.belongs_to      :category, index: true
      t.integer         :views, default: 0

      t.timestamps
    end
  end
end
