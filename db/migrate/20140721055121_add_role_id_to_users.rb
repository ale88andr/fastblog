class AddRoleIdToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :role, index: true
    add_column    :users, :last_signin_at, :datetime
  end
end
