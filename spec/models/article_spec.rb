require 'rails_helper'

describe Article do

  subject { described_class.new }

  context 'database fields' do
    it { expect(subject).to have_db_column(:title).of_type(:string) }
    it { expect(subject).to have_db_column(:contents).of_type(:text) }
    it { expect(subject).to have_db_column(:archive).of_type(:boolean) }
    it { expect(subject).to have_db_column(:author_id).of_type(:integer) }
    it { expect(subject).to have_db_column(:category_id).of_type(:integer) }
    it { expect(subject).to have_db_column(:views).of_type(:integer) }
  end

  context 'assosiation' do
    it { expect(subject).to belong_to(:author).class_name('User').with_foreign_key('author_id') }
    it { expect(subject).to belong_to :category }
    it { expect(subject).to have_many :taggings }
    it { expect(subject).to have_many(:tags).through(:taggings) }
  end

  context 'validation' do
    context 'rules' do
      it { expect(subject).to validate_uniqueness_of :title }
      it { expect(subject).to validate_presence_of :title }
      it { expect(subject).to ensure_length_of(:title).is_at_most(255) }
      it { expect(subject).to validate_presence_of :contents }
    end

    context 'with incorrect data' do
      it { expect(subject).not_to allow_value("false_article_title" * 20).for(:title) }
      it { expect(subject).not_to allow_value(nil).for(:title) }
      it { expect(subject).not_to allow_value(nil).for(:contents) }
      it 'be false on duplicate name' do
        article = create :article
        expect(subject).not_to allow_value(article.title).for(:title)
      end
    end

    context 'with valid data' do
      it { expect(build :article).to be_valid }
    end
  end

  context 'tags' do
    let(:tags)      { "test; articles; tags" }
    let!(:article)  { create :article, all_tags: tags }

    context '#all_tags' do
      it 'must have the same number of tag' do
        expect(article.tags.count).to eq tags.split(';').count
      end
      it 'must have the same sets of tags' do
        expect(article.tags) =~ tags.split(';')
      end
    end
  end
end