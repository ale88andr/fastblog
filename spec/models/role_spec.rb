require 'rails_helper'

describe Role do

  subject { described_class.new }
  
  context 'database fields' do
    it { expect(subject).to have_db_column(:type).of_type(:string) }
  end

  context 'assosiation' do
    it { expect(subject).to have_many :users }
  end
end