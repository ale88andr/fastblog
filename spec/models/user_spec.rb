require 'rails_helper'

describe User do

  subject { described_class.new }

  context 'database fields' do
    it { expect(subject).to have_db_column(:login).of_type(:string) }
    it { expect(subject).to have_db_column(:email).of_type(:string) }
    it { expect(subject).to have_db_column(:password_hash).of_type(:string) }
    it { expect(subject).to have_db_column(:password_salt).of_type(:string) }
    it { expect(subject).to have_db_column(:role_id).of_type(:integer) }
    it { expect(subject).to have_db_column(:last_signin_at).of_type(:datetime) }
  end

  context 'assosiation' do
    it { expect(subject).to belong_to :role }
  end

  context 'validates' do
    context 'rules' do
      it { expect(subject).to validate_uniqueness_of :login }
      it { expect(subject).to validate_uniqueness_of :email }
      it { expect(subject).to ensure_length_of(:login).
                              is_at_most(50).
                              is_at_least(3)
                            }
      it { expect(subject).to ensure_length_of(:password).
                              is_at_least(6) 
                            }
      it { expect(subject).to validate_confirmation_of :password }
    end
  
    context 'with incorrect data' do
      it { expect(subject).not_to allow_value("AbC" * 20).for(:login) }
      it { expect(subject).not_to allow_value("example_email").for(:email) }
      it { expect(subject).not_to allow_value("1a" * 2).for(:password) }
      
      context 'is invalid on duplicate' do

        let(:user) { create :user }

        it "login" do
          expect(subject).not_to allow_value(user.login).for(:login)
        end
        it "email" do
          expect(subject).not_to allow_value(user.login).for(:email)
        end
      end

      context 'is invalid without a' do
        it "login" do
          expect(build :user, login: nil).not_to be_valid
        end
        it "email" do
          expect(build :user, email: nil).not_to be_valid
        end
        it "password" do
          expect(build :user, password: nil).not_to be_valid
        end
      end
    end

    context 'with correct data' do
      it { expect(build :user).to be_valid }
    end
  end

  describe 'instance methods' do

    let(:user) { create :user }

    context '#authenticate' do
      context 'valid user identifiers' do
        subject { described_class.authenticate(user.login, user.password) }

        it { expect(subject).to be  }
        it { expect(subject).to eq user  }
      end

      context 'wrong user identifiers' do
        subject { described_class.authenticate(user.email, "wrong_password") }

        it { expect(subject).not_to be  }
      end
    end

    context '.set_default_role' do
      it { expect(User.find(user).role).to eq Role.find_by(name: 'user') }
    end
  end
end