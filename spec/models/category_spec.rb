require 'rails_helper'

describe Category do

  subject { described_class.new }

  context 'database fields' do
    it { expect(subject).to have_db_column(:name).of_type(:string) }
    it { expect(subject).to have_db_column(:display).of_type(:boolean) }
  end

  context 'assosiation' do
    it { expect(subject).to have_many :articles }
  end

  context 'validation' do
    context 'rules' do
      it { expect(subject).to validate_uniqueness_of :name }
      it { expect(subject).to validate_presence_of :name }
      it { expect(subject).to ensure_length_of(:name).is_at_most(50) }
    end

    context 'with incorrect data' do
      it { expect(subject).not_to allow_value("AbC" * 20).for(:name) }
      it { expect(subject).not_to allow_value(nil).for(:name) }
      it 'be false on duplicate name' do
        category = create :category
        expect(subject).not_to allow_value(category.name).for(:name)
      end
    end

    context 'with valid data' do
      it { expect(build :category).to be_valid }
    end
  end
end