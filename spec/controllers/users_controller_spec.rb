require 'rails_helper'

describe Visitors::UsersController do
  describe 'guest access' do
    describe 'GET #new' do
      before { get :new }

      it "assigns a new User to @user" do
        expect(assigns[:user]).to be_a_new User
      end
      it "render :new template" do
        expect(response).to render_template :new
      end
    end

    describe 'POST #create' do
      context 'with valid attributes' do
        subject { post :create, user: attributes_for(:user) }
        it "saves new user in the database" do
          expect{ 
            subject
          }.to change(User, :count).by(1)
        end
        it "redirects to root" do
          subject
          expect(response).to redirect_to '/'
        end
        it "set flash notice" do
          subject
          expect(flash[:notice]).to be 
        end
      end

      context 'with invalid attributes' do
        subject { post :create, user: attributes_for(:user, email: nil) }
        it "don't saves user in the database" do
          expect{ 
            subject
          }.not_to change(User, :count)
        end
        it "redirects to :new action" do
          subject
          expect(response).to render_template :new
        end
        it "set flash error message" do
          subject
          expect(flash[:danger]).to be
        end
      end
    end
  end
end