require 'rails_helper'

describe Visitors::SessionsController do
  describe 'guest access' do
    describe 'GET #new' do
      before { get :new }

      it "render with :new" do
        expect(response).to render_template :new
      end
    end

    describe 'POST #create' do
      let!(:user) { create :user }
      context 'with valid params' do
        before { post :create, session: { identifier: user.login, password: user.password } }

        it "authenticate user" do
          expect(assigns[:user]).to eq user
        end
        it "create session user_id" do
          expect(session[:user_id]).to eq user.id
        end
        it "redirect to root" do
          expect(response).to redirect_to '/'
        end
        it "sets the flash message" do
          expect(flash[:notice]).to be
        end
        it "updates sign in time" do
          expect(User.find(user).last_signin_at.present?).to eq true
        end
      end

      context 'with invalid params' do
        before { post :create, session: { identifier: nil, password: user.password } }

        it "user authenticate fail" do
          expect(assigns[:user]).not_to be
        end
        it "not create session" do
          expect(session[:user_id]).not_to be
        end
        it "redirects to sign in" do
          expect(response).to redirect_to new_sessions_path
        end
        it "sets the flash error message" do
          expect(flash[:alert]).to be
        end
      end
    end

    describe 'DELETE #destroy' do
      let!(:user) { create :user }
      before { session[:user_id] = user.id }

      it "destroy user session" do
        expect{ delete :destroy }.to change{ session[:user_id] }.from(user.id).to(nil)
      end

      it "redirects to root" do
        delete :destroy
        expect(response).to redirect_to '/'
      end

      it "sets the flash message" do
        delete :destroy
        expect(flash[:notice]).to be
      end
    end
  end
end