require 'rails_helper'

RSpec.describe Admin::Blog::CategoriesController, type: :controller do
  describe "with guest access" do
    let!(:user) { create :user }
    before { auth(user) }

    context '#index' do
      before { get :index }

      it_should_behave_like "access denied for role 'user'"
    end

    context '#delete' do
      before { get :destroy, id: user }

      it_should_behave_like "access denied for role 'user'"
    end

    context '#edit' do
      before { get :edit, id: user }

      it_should_behave_like "access denied for role 'user'"
    end
  end

  describe "with admin access" do
    let(:admin) { create :user, :admin }
    before { auth(admin) }

    describe 'GET #index' do
      let!(:categories) { create_list :category, 3 }
      before { get :index }

      it "populates an array of categories" do
        expect(assigns[:categories]).to match_array categories
      end
      it "renders the :index template" do
        expect(response).to render_template :index
      end
    end

    describe 'GET #new' do
      let(:category) { Category.new }
      before { get :new }

      it "assigns category to @category" do
        expect(assigns[:category]).to be_a_new(Category)
      end
      it "renders the :index template" do
        expect(response).to render_template :new
      end
    end

    describe 'GET #show' do
      let!(:category) { create :category }
      before { get :show, id: category }

      it "assign the requested category to @category" do
        expect(assigns[:category]).to eq category
      end
      it "renders the :edit template" do
        expect(response).to render_template :show
      end
    end

    describe 'GET #edit' do
      let!(:category) { create :category }
      before { get :edit, id: category }

      it "assign the requested category to @category" do
        expect(assigns[:category]).to eq category
      end
      it "renders the :edit template" do
        expect(response).to render_template :edit
      end
    end

    describe 'DELETE #destroy' do
      let!(:category) { create :category }

      it "delete the category" do
        expect{ delete :destroy, id: category }.to change(Category, :count).by(-1)
      end
      it "redirects to :index" do
        delete :destroy, id: category
        expect(response).to redirect_to admin_categories_path
      end
      it "sets flash message" do
        delete :destroy, id: category
        expect(flash[:notice]).to be
      end
    end

    describe 'POST #create' do
      context 'with valid attributes' do
        it "creates a new category" do
          expect{ 
            post :create, category: attributes_for(:category) 
            }.to change(Category, :count).by(1)
        end
        it "redirects to category" do
          post :create, category: attributes_for(:category)
          expect(response).to redirect_to admin_category_url(Category.last)
        end
        it "sets the flash notice" do
          post :create, category: attributes_for(:category)
          expect(flash[:notice]).to be
        end
      end

      context 'with invalid attributes' do
        it "don't creates category" do
          expect{ 
            post :create, category: attributes_for(:category, name: nil) 
            }.not_to change(Category, :count)
        end
        it "redirects to :new" do
          post :create, category: attributes_for(:category, name: nil)
          expect(response).to render_template :new
        end
        it "sets the flash error" do
          post :create, category: attributes_for(:category, name: nil)
          expect(flash[:error]).to be
        end
      end
    end

    describe 'PUT #update' do
      let!(:category) { create :category }

      it "assign the requested category to @category" do
        put :update, id: category, category: attributes_for(:category)
        expect(assigns[:category]).to eq category
      end

      context 'with valid attributes' do

        before { put :update, id: category, category: attributes_for(:category, display: false) }
        
        it 'changes @category attributes' do
          category.reload
          expect(category.display).to eq false
        end

        it 'redirects to :show category' do
          expect(response).to redirect_to admin_category_url(category)
        end

        it 'sets flash notice message' do
          expect(flash[:notice]).to be
        end
      end

      context 'with invalid attributes' do
        let(:new_cat_name) { nil }
        before { put :update, id: category, category: attributes_for(:category, name: new_cat_name) }

        it 'do not change category attributes' do
          category.reload
          expect(category.name).not_to eq new_cat_name
        end

        it 'render :edit template' do
          expect(response).to render_template :edit
        end

        it 'sets flash error message' do
          expect(flash[:error]).to be
        end
      end
    end
  end
end
