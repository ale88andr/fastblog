require 'rails_helper'

describe Admin::Visitors::UsersController do
  describe "with guest access" do
    let!(:user) { create :user }
    before { auth(user) }

    context '#index' do
      before { get :index }

      it_should_behave_like "access denied for role 'user'"
    end

    context '#delete' do
      before { get :destroy, id: user }

      it_should_behave_like "access denied for role 'user'"
    end

    context '#edit' do
      before { get :edit, id: user }

      it_should_behave_like "access denied for role 'user'"
    end
  end

  describe "with admin access" do
    let(:admin) { create :user, role_id: 2 }
    before { auth(admin) }

    context 'GET #index' do
      let!(:users) { create_list :user, 3 }
      before { get :index }

      it "populates an array of users" do
        users << admin
        expect(assigns[:users]).to match_array users
      end
      it "renders the :index template" do
        expect(response).to render_template :index
      end
    end

    context 'GET #edit' do
      let(:exp_user) { create :user }
      before { get :edit, id: exp_user }

      it "assign the requested user to @user" do
        expect(assigns[:user]).to eq exp_user
      end
      it "renders the :edit template" do
        expect(response).to render_template :edit
      end
    end

    context 'PATCH #update' do
      let(:exp_user) { create :user }
      before { patch :update, id: exp_user, user: {role_id: 2} }

      it "changes user role attribute" do
        expect(User.find(exp_user).role_name).to eq 'admin'
      end
      it "redirect to users" do
        expect(response).to redirect_to admin_users_path
      end
      it "sets flash notice" do
        expect(flash[:notice]).to be
      end
    end

    context 'DELETE #destroy' do
      let(:exp_user) { create :user }
      subject { delete :destroy, id: exp_user }

      it "deletes user from database" do
        expect{ subject }.to change(User, :count).by(-1)
      end
      it "redirects to users" do
        expect(subject).to redirect_to admin_users_url
      end
    end
  end
end