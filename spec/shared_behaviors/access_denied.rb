require 'rails_helper'

shared_examples_for "access denied for role 'user'" do

  it "redirect to root" do
    expect(response).to redirect_to root_path
  end
  
  it "set flash error message" do
    expect(flash[:alert]).to be
  end

end