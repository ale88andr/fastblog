FactoryGirl.define do
  factory :article do
    sequence(:title)  { |n| "Article_#{n}" }
    contents          { Faker::Lorem.paragraph }
    views             0
    archive           false

    association       :category, factory: :category
  end

  trait :archived do
    archive           true
  end
end