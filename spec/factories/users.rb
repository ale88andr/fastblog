FactoryGirl.define do
  factory :user do
    login                 { Faker::Name.name }
    email                 { Faker::Internet.email }
    password              'Pa$$w0rd'
    password_confirmation 'Pa$$w0rd'
  end

  trait :admin do
    role_id   2
  end
end