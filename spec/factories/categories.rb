FactoryGirl.define do
  factory :category do
    sequence(:name)   { |n| "Category_#{n}" }
    display           true
  end

  trait :inactive do
    display   false
  end
end