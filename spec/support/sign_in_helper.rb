module SignInHelper
  def auth(user=nil)
    @current_user = user.nil? ? create(:user) : user
    session[:user_id] = @current_user.id
  end

  alias_method :sign_in, :auth
end