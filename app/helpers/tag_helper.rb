module TagHelper

  TAGS_CSS_STYLES = %w{ css1 css2 css3 css4 }

  def tag_cloud(tags)
    max_count_tag = tags.sort_by(&:count).last
    tags.each do |tag|
      index = tag.count.to_f / max_count_tag.count * (TAGS_CSS_STYLES.size - 1)
      yield(tag, TAGS_CSS_STYLES[index.round])
    end
  end
end