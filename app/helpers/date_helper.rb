module DateHelper

  EMPTY_VALUE = 'EMPTY'

  def date_format date
    date.present? ? date.to_s(:default_datetime) : EMPTY_VALUE
  end
end
