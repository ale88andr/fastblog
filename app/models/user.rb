class User < ActiveRecord::Base
  include Searchable

  VALID_EMAIL = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  NEW_USER_DEFAULT_ROLE = 'user'

  # Virtual attributes
  attr_accessor :password

  # Before hooks
  before_save   :encrypt_password
  before_create :set_default_role

  # Assosiation macros
  belongs_to :role
  has_many :articles, foreign_key: 'author_id'

  # Validates macros
  validates :password,  confirmation: true, length: { minimum: 6 }
  validates :login,     length: { minimum: 3, maximum: 50 }, uniqueness: { case_sensitive: false }
  validates :email,     uniqueness: { case_sensitive: false }, format: { with: VALID_EMAIL }

  # Delegate macros
  delegate :name, to: :role, prefix: true

  # Scopes
  scope :list,        -> { select(:id, :login, :role_id, :last_signin_at, :email, :created_at) }
  scope :role,        -> (role) { where role_id: role }
  scope :starts_with, -> (login) { where("login like ?", "#{login}%") }

  # use BCrypt methods
  def encrypt_password
    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
  end

  def set_default_role
    self.role ||= Role.find_by(name: NEW_USER_DEFAULT_ROLE)
  end

  def self.authenticate(user_identifier, password)
    user = User.where("email = ? OR login = ?", user_identifier, user_identifier).first
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
end
