class Category < ActiveRecord::Base
  include DataHandler
  include Searchable

  VALID_CATEGORY_NAME = /\A\w+( -:()_\w+)*\Z/

  # Assosiation macros
  # -----------------------------------------------------------
  has_many :articles

  # Validates macros
  # -----------------------------------------------------------
  validates :name,
            presence:   true,
            length:     { maximum: 50 },
            uniqueness: { case_sensitive: false },
            format:     { with: VALID_CATEGORY_NAME }

  # Callbacks
  # -----------------------------------------------------------
  before_save   :input_data_handle

  # Scopes
  # -----------------------------------------------------------
  scope :display,           -> (display) { where display: display }
  scope :starts_with_name,  -> (name) { where('name like ?', "#{name}%") }

  protected

    def input_data_handle
      self.name = safe_str self.name
    end
end
