class Article < ActiveRecord::Base
  include DataHandler
  include Searchable

  # Assosiation macros
  # -----------------------------------------------------------
  belongs_to :author, class_name: 'User', foreign_key: 'author_id'
  belongs_to :category
  has_many :taggings
  has_many :tags, through: :taggings

  # Validation macros
  # -----------------------------------------------------------
  validates :title,
            presence:   true,
            uniqueness: { case_sensitive: true },
            length:     { maximum: 255 }
  validates :contents,
            presence:   true

  # Scopes
  # -----------------------------------------------------------
  scope :archive,             -> (archive)  { where archive: archive }
  scope :published,           -> (published){ where published: published }
  scope :starts_with_title,   -> (title)    { where('title like ?', "#{title}%") }
  scope :blog,                -> {
    select(:id, :title, :author_id, :created_at, :contents).archive(false).published(true)
  }
  scope :filter,              -> (name) {
    joins(:category).where('categories.name = ?', name) if name.present?
  }

  # Delegator macros
  # -----------------------------------------------------------
  delegate :name, to: :category, prefix: true

  def all_tags=(names)
    self.tags = names.split(';').map do |t|
      Tag.where(name: t.strip).first_or_create!
    end
  end

  def all_tags
    tags.map(&:name)
  end

  def self.tagged_with(name)
    Tag.find_by_name!(name).articles
  end

  def self.tag_counts
    Tag.select('tags.name, count(taggings.tag_id) as count').joins(:taggings).group('taggings.tag_id, tags.name')
  end
end
