class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def user_sign_in?
    current_user.present?
  end

  def is_admin?
    current_user.role_name == 'admin'
  end

  def is_moder?
    current_user.role_name == 'moder'
  end

  def privileged_user?
    is_admin? || is_moder?
  end

  helper_method :current_user, :user_sign_in?, :is_admin?, :is_moder?, :privileged_user?
end
