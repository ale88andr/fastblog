class Blog::ArticlesController < ApplicationController
  respond_to :json

  def index
    articles = Article.blog

    respond_with(articles) do |format|
      format.json { render json: articles.as_json }
    end
  end
end