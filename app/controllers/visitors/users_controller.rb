class Visitors::UsersController < ApplicationController

  respond_to :json, :html

  def new
    @user = User.new 
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_to '/', notice: 'You are now registered user'
    else
      render action: :new
      flash[:danger] = 'An error was ocured!'
    end
  end

  private

    # Strong parameters
    def user_params
      params.require(:user).permit(:login, :email, :password, :password_confirmation)
    end
end