class Visitors::SessionsController < ApplicationController

  def new    
  end

  def create
    # try to authenticate user
    @user = User.authenticate(params[:session][:identifier], params[:session][:password])
    if @user
      @user.update_column(:last_signin_at, Time.now)
      session[:user_id] = @user.id
      redirect_to "/", notice: "You've been logged in."
    else
      redirect_to new_sessions_url, alert: "There was a problem logging you in."
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to "/", notice: "You've been logged out successfully."
  end
end