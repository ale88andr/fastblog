class Admin::Blog::ArticlesController < Admin::ApplicationController
  before_filter only: [:show, :edit, :update, :destroy] { |m| m.find_article params[:id] }

  def new
   @article = Article.new 
  end

  def create
    @article = @current_user.articles.new(article_params)
    if @article.save
      redirect_to admin_article_url(@article), notice: 'Article created!'
    else
      render :new
      flash.now[:error] = 'An error ocured on create article!'
    end
  end

  def show
  end

  def index
    if params[:tag]
      @articles = Article.tagged_with(params[:tag])
    else
      @articles = Article.includes(:category).
        search(params.slice(:archive, :starts_with_title)).
        filter(params[:filter]).
        all
    end
    @categories = Category.all
  end

  def destroy
    @article.destroy
    redirect_to admin_articles_url, notice: 'Article deleted!'
  end

  protected

    def article_params
      params.require(:article).permit(:title, :category_id, :contents, :all_tags)
    end

    def find_article id
      @article = Article.find(id)      
    end
end