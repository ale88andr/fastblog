class Admin::Blog::CategoriesController < Admin::ApplicationController
  before_filter only: [:show, :edit, :update, :destroy] { |m| m.find_category params[:id] }

  def index
    @categories = Category.search(params.slice(:display, :starts_with_name)).all
  end

  def show
  end

  def edit
  end

  def update
    if @category.update_attributes(category_params)
      redirect_to admin_category_url(@category), notice: 'Category updated!'
    else
      render :edit
      flash.now[:error] = "Can't update category attributes!"
    end
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to admin_category_url(@category), notice: 'Category successfully created!'
    else
      render :new
      flash.now[:error] = 'Error on create category!'
    end
  end

  def destroy
    @category.destroy
    redirect_to admin_categories_url, notice: 'Category removed!'
  end

  protected

    def find_category id
      @category = Category.find(id)      
    end

    def category_params
      params.require(:category).permit(:name, :display)
    end
end
