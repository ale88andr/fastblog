class Admin::ApplicationController < ApplicationController
  before_filter :require_privileges

  # Administrative layout template
  layout 'backend'

  def require_privileges
    if !user_sign_in? || current_user.role.name == 'user'
      redirect_to '/', alert: 'Access Denied!'
    end
  end
end