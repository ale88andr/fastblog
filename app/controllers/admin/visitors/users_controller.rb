class Admin::Visitors::UsersController < Admin::Visitors::ApplicationController

  before_filter only: [:destroy, :edit, :update] { |m| m.find_user(params[:id]) }

  def index
    @users = User.search(params.slice(:role, :starts_with)).list
  end

  def edit    
  end

  def update
    if @user.update_column :role_id, params[:user][:role_id]
      redirect_to admin_users_url, notice: "Privileges for user '#{@user.login}' changed to role '#{@user.role_name}' !"
    end
  end

  def destroy
    if @user != @current_user && @current_user.role.name = 'admin'
      @user.destroy
      redirect_to admin_users_url, notice: 'User has been deleted!'
    else
      redirect_to admin_users_url, notice: 'Access denied!'
    end
  end

  protected
    def find_user(id)
      @user = User.find(id)
    end
end