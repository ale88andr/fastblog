class Admin::Visitors::ApplicationController < Admin::ApplicationController
  before_filter :needs_admin_privileges, except: :index

  def needs_admin_privileges
    redirect_to admin_users_url, alert: 'Only admins can manage this resource!' if @current_user.role_name != 'admin'
  end
end