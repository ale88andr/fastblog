@IndexCtrl = ($scope, $location, $http, postData) ->

  $scope.data = postData.data

  postData.loadPosts(null)

  $scope.viewArticle = (articleId) ->
    $location.url('/article/'+articleId)

@IndexCtrl.$inject = ['$scope', '$location', '$http', 'postData']