#= require_self
#= require_tree ./services/main
#= require_tree ./controllers/main

# Creates new Angular module called 'Blog'
Blog = angular.module('Fb', ['ngRoute'])

Blog.config(["$httpProvider", (provider) ->
  provider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')
])

# Sets up routing
Blog.config(['$routeProvider', ($routeProvider) ->
  # Routes for '/articles/'
  $routeProvider
    .when('/article/:articleId', { templateUrl: '../assets/mainArticle.html', controller: 'ArticleCtrl' } )
  # Default
  $routeProvider.otherwise({ templateUrl: '../assets/mainIndex.html', controller: 'IndexCtrl' } )

])