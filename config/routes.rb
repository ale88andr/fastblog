Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  # Admin namespace:
  namespace :admin do
    # (app/controllers/admin/blog/articles_controller.rb)
    scope module: 'blog' do
      resources :articles
      get 'tags/:tag', to: 'articles#index', as: :tag
      resources :categories
    end

    scope module: 'visitors' do
      resources :users
    end

    root 'dashboard#index'
  end

  scope module: 'visitors' do
    resources :users
    resource :sessions, only: [:new, :create, :destroy]
  end

  scope module: 'blog' do
    resources :articles, only: [:index, :show]
  end

  root 'dashboard#index'
end
