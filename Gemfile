source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.4'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Bootstrap framework for sass
gem 'bootstrap-sass', '~> 3.2.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer',  platforms: :ruby
# Use AngularJs
gem 'angularjs-rails'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# Use HAML
gem 'haml-rails'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
# Text editor for blog
gem "ckeditor", github: "galetahub/ckeditor"
# Image uploader
gem 'carrierwave', github: "jnicklas/carrierwave"

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring', group: :development
gem 'puma', group: :development

group :development, :test do
  gem 'rails_dt'
  gem 'ffaker'
  gem 'factory_girl_rails'
end

group :test do
  gem 'rspec-rails'
  gem 'rspec-instafail'
  gem 'rspec-given'
  gem 'rspec-legacy_formatters'
  gem 'capybara', '>= 2.2.0'
  gem 'capybara-puma'
  gem 'shoulda-matchers'
  gem 'guard'
  gem 'guard-rspec'
  gem 'libnotify'
  gem 'fuubar'
  gem 'database_cleaner'
  gem 'simplecov', require: false
end

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]